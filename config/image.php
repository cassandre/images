<?php

return [
    'route' => 'images',
    'path' => storage_path('images'),
    'templates' => array(
        'thumb' => \Meoran\Images\Templates\Thumb::class,
        'small' => \Meoran\Images\Templates\Small::class,
        'medium' => \Meoran\Images\Templates\Medium::class,
        'large' => \Meoran\Images\Templates\Large::class,
        'custom' => \Meoran\Images\Templates\Custom::class,

    ),
    'middlewareAuth' => false,
    'lifetime' => 10,
    'cache' => [
        'path' => storage_path('app')
    ]
];