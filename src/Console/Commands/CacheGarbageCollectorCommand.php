<?php

namespace Meoran\Images\Console\Commands;

use FilesystemIterator;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class CacheGarbageCollectorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:garbage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache garbage collector. Remove useless cache';

    /**
     * CacheGarbageCollectorCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $base = config('image.cache.path');
        if (empty($base)) {
            $this->line("Le cache n'est pas configuré. End...");
            return;
        }
        $fs = new Filesystem();
        $files = collect($fs->allFiles($base));

        /**
         * @var \SplFileInfo $b
         */
        $expiredFileCount = 0;
        $activeFileCount = 0;

        foreach ($files as $file) {
            $time = substr(file_get_contents($file->getPathname()), 0, 10);
            if (!is_numeric($time)) {
                continue;
            }

            if ($time <= time()) {
                unlink($file->getPathname());
                $expiredFileCount++;
            } else {
                $activeFileCount++;

            }
        }

        $this->line('Total expired cache files removed: '.$expiredFileCount);
        $this->line('Total active cache files remaining: '.$activeFileCount);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
