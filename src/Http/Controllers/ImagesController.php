<?php

namespace Meoran\Images\Http\Controllers;

use App\Http\Controllers\Controller;
use Closure;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Meoran\Images\Model\Image;
use Meoran\Images\Templates\Custom;

/**
 * Class ImagesController
 * @package Meoran\Images\Http\Controllers
 */
class ImagesController extends Controller
{

    /**
     * @param $filename
     * @return Application|ResponseFactory|Response
     */
    public function get($filename)
    {
        $template = app('request')->input('template');
        if ($template === null) {
            $template = 'large';
        }
        switch (strtolower($template)) {
            case 'original':
                return $this->getOriginal($filename);
            case 'download':
                return $this->getDownload($filename);
            default:
                return $this->getImage($template, $filename);
        }
    }

    /**
     * @param  Request  $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function upload(Request $request): JsonResponse
    {
        $this->validate($request, [
            'image' => 'required|file|image'
        ]);

        $content = $request->file('image');
        if (empty($content)) {
            $content = $request->input('image');
        }
        $image = new Image(['content' => $content]);
        $image->save();

        return response()->json($image);
    }

    public function delete($id)
    {
        $image = Image::findOrFail($id);
        $image->delete();

        return response()->json("Delete " . $id . " succesfully");
    }

    private function getOriginal($filename)
    {
        $path = $this->getImagePathOrAbort($filename);
        return $this->buildResponse(file_get_contents($path));
    }

    private function getDownload($filename)
    {
        $response = $this->getOriginal($filename);

        return $response->header(
            'Content-Disposition',
            'attachment; filename=' . $filename
        );
    }

    private function getImagePathOrAbort($filename)
    {
        $path = Image::getAbsolutePath($filename);
        if (is_file($path)) {
            return $path;
        }
        abort(404);
    }

    /**
     * @param $template
     * @param $filename
     * @return Application|ResponseFactory|Response
     * @throws BindingResolutionException
     */
    public function getImage($template, $filename)
    {
        $template = $this->getTemplate($template);
        $path = $this->getImagePathOrAbort($filename);

        $lifetime = config('image.lifetime');
        if (empty($lifetime)) {
            $content = $this->applyTemplate($template, $path)->encode();
        } else {
            $content = app('image')->cache(function ($image) use ($template, $path) {
                $this->applyTemplate($template, $path, $image);
                return $image;
            }, $lifetime);
        }

        return $this->buildResponse($content);
    }

    /**
     * @param        $template
     * @param        $path
     * @param  null  $image
     * @return mixed
     * @throws BindingResolutionException
     */
    private function applyTemplate($template, $path, $image = null)
    {
        if (empty($image)) {
            $image = app('image');
        }

        if ($template instanceof Closure) {
            // build from closure callback template
            return $template($image->make($path));
        }

        // build from filter template
        $res = $image->make($path)->filter($template);
        if ($res === null) {
            abort(404);
        }
    }

    /**
     * Returns corresponding template object from given template name
     *
     * @param  string $template
     * @return mixed
     */
    private function getTemplate($templateName)
    {

        $template = config("image.templates.{$templateName}");
        switch (true) {
            // closure template found
            case is_callable($template):
                return $template;

            // filter template found
            case class_exists($template):
                return new $template;

            default:
                return $this->getCustomTemplate($templateName);

        }
    }

    /**
     * @param $content
     * @return Application|ResponseFactory|Response
     */
    private function buildResponse($content)
    {
        // define mime type
        $mime = finfo_buffer(finfo_open(FILEINFO_MIME_TYPE), $content);
        $timeInSecondsToExpire = config('image.lifetime') * 60;

        // return http response
        return response($content, 200, array(
            'Content-Type' => $mime,
            'Cache-Control' => 'max-age=' . $timeInSecondsToExpire . ', public',
            'Expires' => gmdate('D, d M Y H:i:s \G\M\T', time() + $timeInSecondsToExpire),
            'Etag' => md5($content)
        ));
    }

    /**
     * @param $templateName
     * @return Custom
     */
    private function getCustomTemplate($templateName): Custom
    {
        $custom = new Custom();
        $custom->actions = [$templateName];
        return $custom;
    }
}
