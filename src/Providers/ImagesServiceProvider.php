<?php

namespace Meoran\Images\Providers;

use Illuminate\Support\ServiceProvider;
use Intervention\Image\ImageServiceProvider;
use Meoran\Images\Console\Commands\CacheGarbageCollectorCommand;
use Meoran\Images\Console\Commands\RemoveUselessPicturesCommand;
use Spatie\LaravelImageOptimizer\ImageOptimizerServiceProvider;

/**
 * Class ImagesServiceProvider
 * @package Meoran\Images\Providers
 */
class ImagesServiceProvider extends ServiceProvider
{

    public function boot()
    {
        require_once __DIR__ . '/../functions.php';

        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
        $this->mergeConfigFrom(__DIR__ . '/../../config/image.php', 'image');
        $this->mergeConfigFrom(__DIR__ . '/../../config/image-optimizer.php', 'image-optimizer');


        $this->app->register(ImageServiceProvider::class);
        $this->app->register(ImageOptimizerServiceProvider::class);

        $this->loadRoutes();

        if ($this->app->runningInConsole()) {
            $this->publishes([
//                __DIR__ . '/../../config/synchronize.php' => base_path('config/synchronize.php')
            ]);

            $this->commands([
                CacheGarbageCollectorCommand::class,
                RemoveUselessPicturesCommand::class,
            ]);
        }
    }

    public function loadRoutes()
    {
        $this->app->router->group([
            'namespace' => '\Meoran\Images\Http\Controllers',
        ], function ($router) {
            $addAuthMiddleware = config('image.middlewareAuth', false);
            $authMiddleware = [];
            if ($addAuthMiddleware) {
                $authMiddleware = ['middleware' => 'auth'];
            }
            $router->post('images/upload', [
                'as' => 'uploadImage', 'uses' => 'ImagesController@upload'
            ]+$authMiddleware);

            $router->get('images/{filename}', [
                'as' => 'getPicture', 'uses' => 'ImagesController@get'
            ]);

            $router->delete('images/{id}', [
                'as' => 'deletePicture', 'uses' => 'ImagesController@delete'
            ]+$authMiddleware);
        });
    }

}
