<?php

namespace Meoran\Images\Templates;

use Intervention\Image\Constraint;
use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class Small implements FilterInterface
{
    public function applyFilter(Image $image): Image
    {
        return $image->resize(480, null, function (Constraint $constraint) {
            $constraint->upsize();
            $constraint->aspectRatio();
        });
    }
}
