<?php

namespace Meoran\Images\Templates;

use Intervention\Image\Constraint;
use Intervention\Image\Exception\NotSupportedException;
use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

/**
 * Class Custom
 * @package Meoran\Images\Templates
 */
class Custom implements FilterInterface
{
    public $actions;

    /**
     * @param  Image  $image
     * @return Image|null
     */
    public function applyFilter(Image $image): ?Image
    {
        $actions = $this->actions;
        if ($actions === null) {
            $actions = app('request')->input('actions');
        }

        if (empty($actions)) {
            return null;
        }

        foreach ($actions as $action) {
            $exploded = explode(':',$action,2);
            if (isset($exploded[0])) {
                $methodName = $exploded[0];
            }
            $params= [];
            if (isset($exploded[1])) {
                $params = explode(',', $exploded[1]);
            }
            $params[] = static function (Constraint $constraint) {
                $constraint->upsize();
                $constraint->aspectRatio();
            };
            $params = array_map(static function($el) {
                if (is_string($el) && $el === 'null') {
                    return null;
                }
                return $el;
            },$params);
            try {
                call_user_func_array([$image, $methodName], $params);
            } catch (NotSupportedException $exception) {
                return null;
            }
        }

        return $image;
    }
}
