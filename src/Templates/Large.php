<?php

namespace Meoran\Images\Templates;

use Intervention\Image\Constraint;
use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class Large implements FilterInterface
{
    /**
     * @param  Image  $image
     * @return Image
     */
    public function applyFilter(Image $image): Image
    {
        return $image->resize(1920, null, function (Constraint $constraint) {
            $constraint->upsize();
            $constraint->aspectRatio();
        });
    }
}
