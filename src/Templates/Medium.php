<?php

namespace Meoran\Images\Templates;

use Intervention\Image\Constraint;
use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class Medium implements FilterInterface
{
    public function applyFilter(Image $image): Image
    {
        return $image->resize(960, null, function (Constraint $constraint) {
            $constraint->upsize();
            $constraint->aspectRatio();
        });
    }
}
